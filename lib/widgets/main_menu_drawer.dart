import 'package:flutter/material.dart';
import 'package:holidaytrip/view/home/home_view.dart';
import 'package:holidaytrip/view/trips/trips_view.dart';

class MainMenuDrawer extends StatelessWidget {
  const MainMenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Drawer(
      child: ListView(
        children: [
          Container(
            padding: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.indigo,
                  Colors.blueAccent
                ],
                begin: Alignment.topLeft, end: Alignment.bottomRight
              )
            ),
            height: 70,
            child: Column(
              children: const [
                Text("974 L'île Intense", style: TextStyle(color: Colors.white, fontSize: 30),),
              ],
            ),
          ),

          ListTile(leading: const Icon(Icons.home), title: const Text('Accueil'),
            onTap: () => Navigator.pushNamed(context, HomeView.routeName),
          ),
          Container(decoration: const BoxDecoration(border: Border.symmetric(horizontal: BorderSide(width: 0.5, color: Colors.black12)))),
          ListTile(leading: const Icon(Icons.pending_actions), title: const Text('Mes activités'),
            onTap: () {
              Navigator.pushNamed((context), TripsView.routeName);
            },),
          Container(decoration: const BoxDecoration(border: Border.symmetric(horizontal: BorderSide(width: 0.5, color: Colors.black12)))),
        ],
      ),
    );
  }
}
