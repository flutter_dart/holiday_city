import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:holidaytrip/model/city_model.dart';
import '/data/data.dart'as data;

class CityProvider with ChangeNotifier {
  final List<City> _cities = data.cities;

  UnmodifiableListView<City> get cities => UnmodifiableListView(_cities);

  City getCityByName(String cityName) => cities.firstWhere((city) => city.name == cityName);
}