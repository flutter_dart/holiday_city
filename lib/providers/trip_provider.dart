import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:holidaytrip/model/activity_model.dart';
import 'package:holidaytrip/model/trip_model.dart';
import '/data/data.dart'as data;

class TripProvider with ChangeNotifier {
  final List<Trip> _trips = [];

  UnmodifiableListView<Trip> get trips => UnmodifiableListView(_trips);

  void addTrip(Trip trip) {
    _trips.add(trip);
    notifyListeners();
  }

  Trip getById(String id) {
    return trips.firstWhere((trip) => trip.id == id);
  }

  void setActivityDone(Activity activity) {
    activity.status = ActivityStatus.done;
    notifyListeners();
  }
}