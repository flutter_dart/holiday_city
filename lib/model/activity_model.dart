
enum ActivityStatus { onGoing, done }

class Activity {
  String name;
  String image;
  String? id;
  String city;
  String type;
  String time;
  double price;

  ActivityStatus status;

  Activity({
    required this.city,
    required this.image,
    required this.name,
    required this.type,
    required this.price,
    required this.time,
    this.status = ActivityStatus.onGoing,
    this.id,
  }) ;

}