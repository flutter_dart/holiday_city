import 'package:flutter/foundation.dart';
import 'package:holidaytrip/model/activity_model.dart';

class Trip {
  String id = UniqueKey().toString();
  String? city;
  List<Activity> activities;
  DateTime? date;
  Trip({
    required this.city,
    required this.activities,
    required this.date
  });

}