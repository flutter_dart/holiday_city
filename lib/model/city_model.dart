import 'package:holidaytrip/model/activity_model.dart';

class City {
  late String image;
  late String name;
  List<Activity> activities;
  City({required this.image, required this.name, required this.activities});
}