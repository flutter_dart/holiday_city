import 'package:flutter/material.dart';
import 'package:holidaytrip/model/activity_model.dart';
import 'package:holidaytrip/view/trip/widgets/trip_activity_list.dart';

class TripActivities extends StatelessWidget {
  final String tripId;
  const TripActivities({Key? key, required this.tripId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        children: [
          Container(
            color: Colors.indigo,
            child: const TabBar(tabs: [
              Tab(text: 'En cours'),
              Tab(text: 'Terminé',)
            ],
            indicatorColor: Colors.redAccent,
            ),
          ),
          SizedBox(
            height: 600,
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
                children: [
                  TripActivityList(
                   tripId: tripId,
                    filter: ActivityStatus.onGoing,),
                  TripActivityList(
                    tripId: tripId,
                    filter: ActivityStatus.done,)
                ]
            ),
          )
        ],
      ),
    );
  }
}
