import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:holidaytrip/model/activity_model.dart';
import 'package:holidaytrip/model/trip_model.dart';
import 'package:holidaytrip/providers/trip_provider.dart';
import 'package:provider/provider.dart';

class TripActivityList extends StatelessWidget {
  final String tripId;
  final ActivityStatus filter;
  const TripActivityList({Key? key, required this.filter, required this.tripId}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Consumer<TripProvider>(builder: (context, tripProvider, child) {

      final Trip trip = Provider.of<TripProvider>(context).getById(tripId);
      final List<Activity> activities = trip.activities.where(
              (activity) => activity.status == filter
      ).toList();

      return ListView.builder(
          itemCount: activities.length,
          itemBuilder: (context, i) {
        Activity activity = activities[i];
        return Container(
          margin:  const EdgeInsets.symmetric(horizontal: 10),
          child: (filter == ActivityStatus.onGoing) ? Dismissible(
            direction: DismissDirection.endToStart,
            background: Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              alignment: Alignment.centerRight,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.green,
              ),
              child: const Icon(Icons.check, color: Colors.white, size: 30,),
            ),
            key: ValueKey(activity.id),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                      leading: CircleAvatar(backgroundImage: AssetImage(activity.image)),
                      title: Text(activity.name, style: const TextStyle(fontSize: 20)),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text('Activité : ${activity.type}', style: const TextStyle(fontSize: 15)),
                          Text('Durée : ${activity.time}', style: const TextStyle(fontSize: 10))
                        ],
                      )
                  ),
                ],
              )
            ),
            onDismissed: (_) {
              Provider.of<TripProvider>(context, listen: false).setActivityDone(activity);
            },
          )
              :
          Card(
              child: ListTile(
                title: Text(
                    activity.name,
                    style: const TextStyle(color: Colors.grey)
                ),
              )),
        );
        },
      );
      });
  }
}
