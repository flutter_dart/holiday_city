import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:holidaytrip/model/city_model.dart';
import 'package:holidaytrip/model/trip_model.dart';
import 'package:holidaytrip/providers/city_provider.dart';
import 'package:holidaytrip/providers/trip_provider.dart';
import 'package:holidaytrip/view/city/widgets/activity_list.dart';
import 'package:holidaytrip/view/city/widgets/trip_activity_list.dart';
import 'package:holidaytrip/view/city/widgets/trip_overview.dart';
import 'package:holidaytrip/model/activity_model.dart';
import 'package:holidaytrip/view/home/home_view.dart';
import 'package:holidaytrip/widgets/main_menu_drawer.dart';
import 'package:provider/provider.dart';


class CityView extends StatefulWidget {
  static const String routeName = '/city';

  const CityView({Key? key}) : super(key: key);

  @override
  State<CityView> createState() => _CityViewState();

}

class _CityViewState extends State<CityView> {

  late Trip myTrip;
  late int index;


  @override
  void initState() {
    myTrip = Trip(activities: [], city: null, date: null);
    index = 0;
  }

  void setDate() {
    showDatePicker(
        context: context,
        initialDate: DateTime.now().add(const Duration(days: 1)),
        firstDate: DateTime.now(),
        lastDate: DateTime(2023)
    ).then((newDate) => {
      if(newDate != null) {
        setState(()=> {
          myTrip.date = newDate
        })
      }
    });
  }

  void switchIndex(newIndex) {
    setState(() {
      index = newIndex;
    });
  }

  void toggleActivity(Activity activity) {
    setState(() {
      myTrip.activities.contains(activity)
          ? myTrip.activities.remove(activity)
          : myTrip.activities.add(activity);
    });
  }

  void deleteTripActivity(Activity activity) {
    setState(() {
      myTrip.activities.remove(activity);
    });
  }

  void saveTrip(String cityName) async {
    final result =  await showDialog (
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Container(
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.all(Radius.circular(5))
              ),
                child: const Text(
                    'Voulez vous sauvegarder ?',
                    style: TextStyle(color: Colors.white))),
            contentPadding: const EdgeInsets.all(30),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  OutlinedButton(
                    child: const Text('annuler'),
                    onPressed: () {
                      Navigator.pop(context, 'cancel');
                    },
                  ),
                  const SizedBox(width: 10,),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context, 'save');
                    },
                    child: const Icon(Icons.save),
                  )
                ],
              )
            ],
          );
        },
    );
    if (myTrip.date == null) {
      showDialog(context: context, builder: (context) {
        return AlertDialog(
          title: Container(
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(color: Colors.amber),
            child: const Text('Attention'),
             ),
          content: const Text('Vous n\'avez pas entrée de date'),
          actions: [
            TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Text("ok"))
          ],
        );
      });
    } else if (result == 'save') {
      // widget.addTrip(myTrip);
      myTrip.city = cityName;
      if (!mounted) return;
      Provider.of<TripProvider>(context, listen: false).addTrip(myTrip);
      Navigator.pushNamed(context, HomeView.routeName);
    }
  }

  double get amount {
    return myTrip.activities.fold(0.00, (previousValue, activity) {
      return num.parse((previousValue + activity.price).toStringAsFixed(2)) as double;
    });
  }

  @override
  Widget build(BuildContext context) {
    String cityName = ModalRoute.of(context)?.settings.arguments as String;
    City city = Provider.of<CityProvider>(context).getCityByName(cityName);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: const Text("Budgetiser votre journée"),
      ),
      drawer: const MainMenuDrawer(),
      body: Column(
        children: [
          TripOverView(
              myTrip: myTrip,
              setDate: setDate,
              cityName: city.name,
              amount: amount,
          ),
          Expanded(child: (index == 0) ? ActivityList(
            activities: city.activities,
            selectedActivities: myTrip.activities,
            toggleActivity: toggleActivity,
          )
              : TripActivityList(
              activities: myTrip.activities,
              deleteTripActivity: deleteTripActivity
          )
          )]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          saveTrip(city.name);
        },
        backgroundColor: Colors.purple,
        child: const Icon(Icons.save),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.map), label: "Découverte"),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: "Mes activité"),
        ],
        onTap: switchIndex,
      ),
    );
  }
}

