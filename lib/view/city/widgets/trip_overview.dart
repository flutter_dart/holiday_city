import 'package:flutter/material.dart';
import 'package:holidaytrip/model/trip_model.dart';
import 'package:intl/intl.dart';

class TripOverView extends StatelessWidget {

  final Trip myTrip;
  final VoidCallback setDate;
  final String cityName;
  final double? amount;

  const TripOverView({
    Key? key,
    required this.myTrip,
    required this.setDate,
    required this.cityName,
    this.amount
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return           Container(
        padding: const EdgeInsets.all(10),
        height: 180,
        color: Colors.white,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(10),
                color: Colors.black12,
                child: SizedBox(
                    child: Text(
                      cityName,
                      style: const TextStyle(
                        fontSize: 25,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
              ),
              const SizedBox(height: 10,),
              Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                          myTrip.date != null
                              ? DateFormat('d/M/y').format(myTrip.date!)
                              : 'Aucune date de sélectionnée',
                          style: myTrip.date != null
                              ? const TextStyle(fontSize: 15, color: Colors.black87)
                              : const TextStyle(fontSize: 15, color: Colors.redAccent),
                        )
                    ),
                    ElevatedButton(
                      onPressed: setDate,
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.amber)
                      ),
                      child: const Icon(Icons.date_range,),
                    )]),
              const SizedBox(height: 20,),
              Row(
                  children: [
                    const Expanded(
                        child: Text(
                          'Montant/personne : ',
                          style: TextStyle(fontSize: 20),
                        )),
                    Text(
                      '$amount \$',
                      style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    )
                  ]
              ),
            ]
        ));
  }
}
