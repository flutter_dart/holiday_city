import 'package:flutter/material.dart';
import 'package:holidaytrip/model/activity_model.dart';

class TripActivityList extends StatelessWidget {
  final List<Activity> activities;
  final Function deleteTripActivity;

  const TripActivityList({
    Key? key,
    required this.activities,
    required this.deleteTripActivity
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        var activity = activities[index];
        return Card(
          child: ListTile(
              leading: CircleAvatar(backgroundImage: AssetImage(activity.image)),
              title: Text(activity.name),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('Activité : ${activity.type}', style: const TextStyle(fontSize: 15)),
                  Text('Durée : ${activity.time}', style: const TextStyle(fontSize: 10))
                ]
              ),
              trailing: IconButton(
                icon: const Icon(Icons.delete, color: Colors.red,),
                onPressed: () {
                  deleteTripActivity(activity);
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content:  Text('Activité supprimé'),
                        backgroundColor: Colors.red,
                        duration: Duration(seconds: 1)
                      )
                  );
                },
              )
            ),

        );},
      itemCount: activities.length,
    );
  }
}
