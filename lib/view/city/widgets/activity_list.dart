import 'package:flutter/material.dart';
import 'package:holidaytrip/model/activity_model.dart';
import 'package:holidaytrip/view/city/widgets/activity_card.dart';

class ActivityList extends StatelessWidget {

  final List<Activity> activities;
  final List<Activity> selectedActivities;
  final Function toggleActivity;

  const ActivityList({
    Key? key,
    required this.activities,
    required this.selectedActivities,
    required this.toggleActivity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        crossAxisCount: 2,
        children: activities.map(
                (activity) => ActivityCard(
                    activity: activity,
                    isSelected: selectedActivities.contains(activity),
                    toggleActivity: () => toggleActivity(activity)
                )
        ).toList()
    );
  }
}
