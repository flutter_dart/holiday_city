import 'package:flutter/material.dart';
import 'package:holidaytrip/model/trip_model.dart';
import 'package:holidaytrip/providers/trip_provider.dart';
import 'package:holidaytrip/view/trips/widgets/trip_list.dart';
import 'package:holidaytrip/widgets/main_menu_drawer.dart';
import 'package:provider/provider.dart';

class TripsView extends StatefulWidget {
  static const String routeName = '/trips';

  const TripsView({Key? key}) : super(key: key);


  @override
  State<TripsView> createState() => _TripsViewState();
}

class _TripsViewState extends State<TripsView> {
  @override
  Widget build(BuildContext context) {

    List<Trip> trips = Provider.of<TripProvider>(context).trips;

    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.indigo,
            title: const Text('Mes activités'),
            bottom: const TabBar(
                tabs: [
                  Tab(text: 'A venir'),
                  Tab(text: 'Passés',)
                ],
            ),
          ),
          body: TabBarView(
            children: [
              TripList(
                trips: trips
                    .where(
                        (trip) => DateTime.now().isBefore(trip.date!)
                ).toList(),
              ),
              TripList(
                trips: trips
                    .where(
                        (trip) => DateTime.now().isAfter(trip.date!)
                ).toList(),
              ),
            ],

          ),
          drawer: const MainMenuDrawer(),
        )
    );
  }
}
