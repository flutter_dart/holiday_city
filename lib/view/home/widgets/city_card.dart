import 'package:flutter/material.dart';
import 'package:holidaytrip/model/city_model.dart';

class CityCard extends StatelessWidget {

  final City city;

  const CityCard({
    required this.city,
    Key? key
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 10,
        child: SizedBox(
          height: 180,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Ink.image( // @DOC : A convenience widget for drawing images and
                         // other decorations on Material widgets
                         // link : https://api.flutter.dev/flutter/material/Ink-class.html
                fit: BoxFit.cover,
                image: AssetImage(city.image),
                child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                          context,
                        '/city',
                        arguments: city.name
                      );
                    },
                ),
              ),
              Container (
                padding: const EdgeInsets.all(10),
                  width: 55,
                  alignment: Alignment.topRight,
                  child: SizedBox(
                      child : TextButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.black45),
                          ),
                          onPressed: () { Navigator.pushNamed(
                              context,
                              '/city',
                              arguments: city.name
                          ); },
                          child: Row(
                              children: [
                                Text(
                                  city.name,
                                  style: const TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),
                              ]
                          )
                      )
                  )
              )
            ]
          )
        )
    );
  }
}

