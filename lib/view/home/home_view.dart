import 'package:flutter/material.dart';
import 'package:holidaytrip/model/city_model.dart';
import 'package:holidaytrip/providers/city_provider.dart';
import 'package:holidaytrip/view/home/widgets/city_card.dart';
import 'package:holidaytrip/widgets/main_menu_drawer.dart';
import 'package:provider/provider.dart';

import '../../data/data.dart';


class HomeView extends StatefulWidget {
  static const String routeName = '/';

  const HomeView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeViewState();
  }

}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    List<City> cities = Provider.of<CityProvider>(context).cities;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: const Text("974 L'île Intense"),
        actions: const [Icon(Icons.more_vert)],
      ),
      drawer: const MainMenuDrawer(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ...cities.map((city) {
              return CityCard(
                city: city
            );
          }).toList()] ,
        ),
      ),
    );
  }
}
