import 'package:flutter/material.dart';
import 'package:holidaytrip/providers/city_provider.dart';
import 'package:holidaytrip/providers/trip_provider.dart';
import 'package:holidaytrip/view/city/city_view.dart';
import 'package:holidaytrip/view/error/not_found.dart';
import 'package:holidaytrip/view/home/home_view.dart';
import 'package:holidaytrip/view/trip/trip_view.dart';
import 'package:holidaytrip/view/trips/trips_view.dart';
import 'package:provider/provider.dart';


void main() {
  runApp(const HolidayTrip());
}

@immutable
class HolidayTrip extends StatefulWidget {
  const HolidayTrip({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HolidayTripState();

}

class _HolidayTripState extends State<HolidayTrip> {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: CityProvider()),
        ChangeNotifierProvider.value(value: TripProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: const HomeView(),
        routes: {
          CityView.routeName: (_) => const CityView(),
          TripsView.routeName: (_) => const TripsView(),
          TripView.routeName: (_) => const TripView()
        },
        onUnknownRoute: (_) => MaterialPageRoute(builder: (context) => const NotFound()),
    ));
  }

}

